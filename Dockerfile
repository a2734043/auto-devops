FROM python:3.5-alpine
ENV PYTHONUNBUFFERED 1 
ADD /project /project 
RUN mkdir /config  
ADD /config/requirements.txt /config/  
RUN pip install -r /config/requirements.txt 
WORKDIR /project/mysite
EXPOSE 5000
CMD ["python", "manage.py", "runserver", "0.0.0.0:5000"]
